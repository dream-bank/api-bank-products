import {AccountRepositoryMockSpy} from "../mocks/account.repository.mock.spy";
import {ApplicationException} from "../../../src/common/exceptions/application.exception";
import {TransactionRepositoryMockSpy} from "../mocks/transaction.repository.mock.spy";
import {TransactionUseCase} from "../../../src/domain/use-cases/transaction/transaction.usecase";
import {transactionDataMock, transactionDetailDataMock} from "../mocks/data/transaction.data.mock";

const transactionRepositoryMockSpy = new TransactionRepositoryMockSpy();
const accountRepositoryMock = new AccountRepositoryMockSpy();
const transactionUseCase = new TransactionUseCase(transactionRepositoryMockSpy, accountRepositoryMock);

describe("Get transactions by account id", () => {
    it("should return transactions array if account have transactions", async () => {
        const transactions = await transactionUseCase.getTransactionsByAccountId("1");
        expect(transactions).toEqual([transactionDataMock]);
    });

    it("should return application exception if account doesn't have any transaction", async () => {
        try {
            await transactionUseCase.getTransactionsByAccountId("21");
        } catch (error) {
            expect(error).toBeInstanceOf(ApplicationException);
        }
    });

    it("should return application exception if accountId is not valid", async () => {
        try {
            await transactionUseCase.getTransactionsByAccountId("");
        } catch (error) {
            expect(error).toBeInstanceOf(ApplicationException);
        }
    });
});

describe("Get transactions detail by transaction id", () => {
    it("should return transaction detail if transaction have details", async () => {
        const transactionDetail = await transactionUseCase.getTransactionsDetail("1");
        expect(transactionDetail).toEqual(transactionDetailDataMock);
    });

    it("should return application exception if transaction doesn't have any detail", async () => {
        try {
            await transactionUseCase.getTransactionsDetail("21");
        } catch (error) {
            expect(error).toBeInstanceOf(ApplicationException);
        }
    });

    it("should return application exception if transactionId is not valid", async () => {
        try {
            await transactionUseCase.getTransactionsDetail("");
        } catch (error) {
            expect(error).toBeInstanceOf(ApplicationException);
        }
    });
});

describe("Get average transactions amount by account id", () => {
    it("should return average transactions amount if input data is correct", async () => {
        const average = await transactionUseCase.getAverageTransactionsAmountByAccountId("1",undefined,undefined);
        expect(average).toEqual(2500);
    });

    it("should return application exception if start date is incorrect", async () => {
        try {
            await transactionUseCase.getAverageTransactionsAmountByAccountId("1","2020",undefined);
        } catch (error) {
            expect(error).toBeInstanceOf(ApplicationException);
        }
    });

    it("should return application exception if end date is incorrect", async () => {
        try {
            await transactionUseCase.getAverageTransactionsAmountByAccountId("1",undefined,"2020");
        } catch (error) {
            expect(error).toBeInstanceOf(ApplicationException);
        }
    });

    it("should return application exception if accountId is not valid", async () => {
        try {
            await transactionUseCase.getAverageTransactionsAmountByAccountId("",undefined,undefined);
        } catch (error) {
            expect(error).toBeInstanceOf(ApplicationException);
        }
    });
});
