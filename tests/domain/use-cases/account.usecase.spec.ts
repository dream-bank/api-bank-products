import {AccountUseCase} from "../../../src/domain/use-cases/account/account.usecase";
import {AccountRepositoryMockSpy} from "../mocks/account.repository.mock.spy";
import {ApplicationException} from "../../../src/common/exceptions/application.exception";
import {accountDataMock} from "../mocks/data/account.data.mock";

const accountRepositoryMock = new AccountRepositoryMockSpy();
const accountUseCase = new AccountUseCase(accountRepositoryMock);

describe("Get accounts by user id", () => {
    it("should return accounts array if user have accounts", async () => {
        const accounts = await accountUseCase.getAccountsByUserId("123");
        expect(accounts).toEqual([accountDataMock]);
    });

    it("should return application exception if user doesn't have any account", async () => {
        try {
            await accountUseCase.getAccountsByUserId("1");
        } catch (error) {
            expect(error).toBeInstanceOf(ApplicationException);
        }
    });

    it("should return application exception if userId is not valid", async () => {
        try {
            await accountUseCase.getAccountsByUserId("");
        } catch (error) {
            expect(error).toBeInstanceOf(ApplicationException);
        }
    });
});
