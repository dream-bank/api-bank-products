import {ProductRepositoryMockSpy} from "../mocks/product.repository.mock.spy";
import {ProductUseCase} from "../../../src/domain/use-cases/product/product.usecase";
import {productDataMock} from "../mocks/data/product.data.mock";

const productRepositoryMockSpy = new ProductRepositoryMockSpy();
const productUseCase = new ProductUseCase(productRepositoryMockSpy);

describe("Get products", () => {
    it("should return products array", async () => {
        const accounts = await productUseCase.getProducts();
        expect(accounts).toEqual([productDataMock]);
    });
});
