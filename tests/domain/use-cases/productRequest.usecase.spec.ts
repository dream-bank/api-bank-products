import {ProductRequestUseCase} from "../../../src/domain/use-cases/product-request/productRequest.usecase";
import {ProductRequestRepositoryMockSpy} from "../mocks/productRequest.repository.mock.spy";
import {ProductRepositoryMockSpy} from "../mocks/product.repository.mock.spy";
import {AccountRepositoryMockSpy} from "../mocks/account.repository.mock.spy";
import {TransactionRepositoryMockSpy} from "../mocks/transaction.repository.mock.spy";
import {
    productRequestDataMock,
    productRequestDtoInvalidDataMock,
    productRequestDtoValidDataMock, productRequestDtoWithNonExistentProductTypeDataMock
} from "../mocks/data/productRequest.data.mock";
import {ApplicationException} from "../../../src/common/exceptions/application.exception";

const productRequestRepositoryMockSpy = new ProductRequestRepositoryMockSpy();
const productRepositoryMockSpy = new ProductRepositoryMockSpy();
const accountRepositoryMock = new AccountRepositoryMockSpy();
const transactionRepositoryMockSpy = new TransactionRepositoryMockSpy();

const productUseCase = new ProductRequestUseCase(
    productRequestRepositoryMockSpy,
    productRepositoryMockSpy,
    accountRepositoryMock,
    transactionRepositoryMockSpy
    );

describe("Create product request", () => {
    it("should return a product request with all user products and accounts transactions", async () => {
        const accounts = await productUseCase.createProductRequest(productRequestDtoValidDataMock);
        expect(accounts).toEqual(productRequestDataMock);
    });

    it("should return a product request with all user products and accounts transactions", async () => {
        const accounts = await productUseCase.createProductRequest(productRequestDtoValidDataMock);
        expect(accounts).toEqual(productRequestDataMock);
    });

    it("should return application exception if productRequestDto is invalid", async () => {
        try {
            await productUseCase.createProductRequest(productRequestDtoInvalidDataMock);
        } catch (error) {
            expect(error).toBeInstanceOf(ApplicationException);
        }
    });

    it("should return application exception if productRequestDto is invalid", async () => {
        try {
            await productUseCase.createProductRequest(productRequestDtoWithNonExistentProductTypeDataMock);
        } catch (error) {
            expect(error).toBeInstanceOf(ApplicationException);
        }
    });
});
