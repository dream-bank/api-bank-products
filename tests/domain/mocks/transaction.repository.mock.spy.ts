import {TransactionRepository} from "../../../src/domain/models/transaction/gateways/transaction.repository";
import {Transaction, TransactionDetail} from "../../../src/domain/models/transaction/transaction";
import {TransactionAmountsDto} from "../../../src/common/dtos/transaction.dto";
import {
    transactionAmountsDtoDataMock1, transactionAmountsDtoDataMock2,
    transactionDataMock,
    transactionDetailDataMock
} from "./data/transaction.data.mock";
import {accountDataMock} from "./data/account.data.mock";

export class TransactionRepositoryMockSpy implements TransactionRepository {
    async getTransactionsByAccountId(accountId: string): Promise<Transaction[] | null> {
        if (accountId) {
            const transactions: Transaction[] = [];
            if (accountId === String(accountDataMock.id)) {
                transactions.push(transactionDataMock);
                return transactions;
            }
        }
        return null;
    }

    async getTransactionsById(transactionId: string): Promise<Transaction | null> {
        if (transactionId) {
            if (transactionId === String(transactionDataMock.id)) {
                return transactionDataMock;
            }
        }
        return null;
    }

    async getTransactionsDetail(transactionId: string): Promise<TransactionDetail | null> {
        if (transactionId) {
            if (transactionId === String(transactionDataMock.id)) {
                return transactionDetailDataMock;
            }
        }
        return null;
    }

    async getValidTransactionsAmountsByAccountIdAndDate(
        accountId: string, startDate: string | undefined, endDate: string | undefined)
        : Promise<TransactionAmountsDto[] | null> {
        if (accountId) {
            const TransactionAmountsDto: TransactionAmountsDto[] = [];
            if (accountId === String(accountDataMock.id)) {
                TransactionAmountsDto.push(transactionAmountsDtoDataMock1);
                TransactionAmountsDto.push(transactionAmountsDtoDataMock2);
                return TransactionAmountsDto;
            }
        }
        return null;
    }
}
