import {AccountRepository} from "../../../src/domain/models/account/gateways/account.repository";
import {Account} from "../../../src/domain/models/account/account";
import {accountDataMock} from "./data/account.data.mock";

export class AccountRepositoryMockSpy implements AccountRepository {
    async getAccountById(accountId: string): Promise<Account | null> {
        if (accountId) {
            if (accountId === String(accountDataMock.id)) {
                return accountDataMock;
            }
        }
        return null;
    }

    async getAccountsByUserId(userId: string): Promise<Account[] | null> {
        if (userId) {
            const accounts: Account[] = [];
            if (userId === accountDataMock.userId) {
                accounts.push(accountDataMock);
            }
            return accounts;
        }
        return null;
    }
}
