import {Transaction, TransactionDetail} from "../../../../src/domain/models/transaction/transaction";
import {TransactionAmountsDto} from "../../../../src/common/dtos/transaction.dto";

export const transactionDataMock: Transaction = {
    id: 1,
    account: 1,
    status: "Aprobada",
    commerce: "Store ABC",
    amount: 2000,
    createdAt: new Date(),
    updatedAt: null
}

export const transactionDetailDataMock: TransactionDetail = {
    id: 1,
    status: "Aprobada",
    commerce: "Store ABC",
    tax: 3000,
    description: "Some description",
    amount: 2000,
    createdAt: new Date(),
    updatedAt: null
}

export const transactionAmountsDtoDataMock1: TransactionAmountsDto = {
    amount:  2000,
}

export const transactionAmountsDtoDataMock2: TransactionAmountsDto = {
    amount:  3000,
}
