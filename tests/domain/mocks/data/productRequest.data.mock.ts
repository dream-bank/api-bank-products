import {ProductRequest} from "../../../../src/domain/models/product-request/productRequest";
import {ProductRequestDto} from "../../../../src/common/dtos/productRequest.dto";

export const productRequestDataMock: ProductRequest = {
    id: 1,
    userId: "123",
    status: "Aprobada",
    productType: "1",
    products: [],
    transactions: [],
    createdAt: new Date(),
    updatedAt: null
}

export const productRequestDtoValidDataMock: ProductRequestDto = {
    userId: "1",
    productType: "1"
}

export const productRequestDtoInvalidDataMock: ProductRequestDto = {
    userId: "1",
    productType: ""
}

export const productRequestDtoWithNonExistentProductTypeDataMock: ProductRequestDto = {
    userId: "1",
    productType: "23"
}
