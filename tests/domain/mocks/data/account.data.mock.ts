import {Account} from "../../../../src/domain/models/account/account";

export const accountDataMock: Account = {
    id: 1,
    userId: "123",
    balance: 29000,
    productType: "cuenta de ahorros",
    createdAt: new Date(),
    updatedAt: null
}
