import {Product} from "../../../../src/domain/models/product/product";

export const productDataMock: Product = {
    id: 1,
    name: "Cuenta de ahorros",
    createdAt: new Date(),
    updatedAt: null
}
