import {ProductRepository} from "../../../src/domain/models/product/gateways/product.repository";
import {Product} from "../../../src/domain/models/product/product";
import {productDataMock} from "./data/product.data.mock";
import {productRequestDataMock} from "./data/productRequest.data.mock";

export class ProductRepositoryMockSpy implements ProductRepository {
    async getProductById(productId: string): Promise<Product | null> {
        if (productId) {
            if (productId === String(productDataMock.id)) {
                return productDataMock;
            }
        }
        return null;
    }

    async getProducts(): Promise<Product[]> {
        return [productDataMock];
    }

    async getProductsApprovedByUserId(userId: string): Promise<Product[]> {
        const products: Product[] = [];
        if (userId) {
            if (userId === String(productRequestDataMock.userId)) {
                products.push(productDataMock)
                return products;
            }
        }
        return products;
    }
}
