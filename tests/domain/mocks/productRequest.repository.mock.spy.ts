import {productRequestDataMock} from "./data/productRequest.data.mock";
import {ProductRequestRepository} from "../../../src/domain/models/product-request/gateways/productRequest.repository";
import {ProductRequestDto} from "../../../src/common/dtos/productRequest.dto";
import {ProductRequest} from "../../../src/domain/models/product-request/productRequest";

export class ProductRequestRepositoryMockSpy implements ProductRequestRepository {

    async createProductRequest(productRequest: ProductRequestDto): Promise<ProductRequest> {
        return productRequestDataMock;
    }
}
