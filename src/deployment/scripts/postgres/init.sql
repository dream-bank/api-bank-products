DROP TABLE IF EXISTS "accounts";
DROP SEQUENCE IF EXISTS accounts_id_seq;
CREATE SEQUENCE accounts_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."accounts"
(
    "id"              integer   DEFAULT nextval('accounts_id_seq') NOT NULL,
    "balance"         numeric                                      NOT NULL,
    "user_id"         character varying(20)                        NOT NULL,
    "created_at"      timestamp DEFAULT now()                      NOT NULL,
    "updated_at"      timestamp DEFAULT now(),
    "product_type_id" integer,
    CONSTRAINT "PK_5a7a02c20412299d198e097a8fe" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "accounts" ("id", "balance", "user_id", "created_at", "updated_at", "product_type_id")
VALUES (1, 6000000, '1085933225', '2021-06-20 23:55:00.645998', '2021-06-20 23:55:00.645998', 1),
       (2, 120000, '123456', '2021-06-21 00:21:33.119706', '2021-06-21 00:21:33.119706', 2),
       (3, 450000, '1', '2021-06-21 13:35:33.111703', '2021-06-21 13:35:33.111703', 2);

DROP TABLE IF EXISTS "commerces";
DROP SEQUENCE IF EXISTS commerces_id_seq;
CREATE SEQUENCE commerces_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."commerces"
(
    "id"         integer   DEFAULT nextval('commerces_id_seq') NOT NULL,
    "name"       character varying                             NOT NULL,
    "created_at" timestamp DEFAULT now()                       NOT NULL,
    "updated_at" timestamp DEFAULT now(),
    CONSTRAINT "PK_f589550ef99c2016fbfface36d3" PRIMARY KEY ("id"),
    CONSTRAINT "UQ_a579715488400582f8151ea9d24" UNIQUE ("name")
) WITH (oids = false);

INSERT INTO "commerces" ("id", "name", "created_at", "updated_at")
VALUES (1, 'Tienda XYZ', '2021-06-20 23:59:26.58232', '2021-06-20 23:59:26.58232');

DROP TABLE IF EXISTS "product_request_status";
DROP SEQUENCE IF EXISTS product_request_status_id_seq;
CREATE SEQUENCE product_request_status_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."product_request_status"
(
    "id"         integer   DEFAULT nextval('product_request_status_id_seq') NOT NULL,
    "name"       character varying                                          NOT NULL,
    "created_at" timestamp DEFAULT now()                                    NOT NULL,
    "updated_at" timestamp DEFAULT now(),
    CONSTRAINT "PK_f4cfdb867430461047467319948" PRIMARY KEY ("id"),
    CONSTRAINT "UQ_430f2e21f885254cac9fa4d33d9" UNIQUE ("name")
) WITH (oids = false);

INSERT INTO "product_request_status" ("id", "name", "created_at", "updated_at")
VALUES (1, 'Pendiente', '2021-06-21 14:47:55.132575', '2021-06-21 14:47:55.132575'),
       (2, 'Aprobada', '2021-06-21 14:48:06.329437', '2021-06-21 14:48:06.329437'),
       (3, 'Rechazada', '2021-06-21 14:48:15.481', '2021-06-21 14:48:15.481');

DROP TABLE IF EXISTS "product_requests";
DROP SEQUENCE IF EXISTS product_requests_id_seq;
CREATE SEQUENCE product_requests_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."product_requests"
(
    "id"                        integer   DEFAULT nextval('product_requests_id_seq') NOT NULL,
    "user_id"                   character varying(20)                                NOT NULL,
    "created_at"                timestamp DEFAULT now()                              NOT NULL,
    "updated_at"                timestamp DEFAULT now(),
    "product_request_status_id" integer,
    "product_type_id"           integer,
    CONSTRAINT "PK_95548600cf27dcc824033dea8cd" PRIMARY KEY ("id")
) WITH (oids = false);

DROP TABLE IF EXISTS "product_types";
DROP SEQUENCE IF EXISTS product_types_id_seq;
CREATE SEQUENCE product_types_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."product_types"
(
    "id"         integer   DEFAULT nextval('product_types_id_seq') NOT NULL,
    "name"       character varying                                 NOT NULL,
    "created_at" timestamp DEFAULT now()                           NOT NULL,
    "updated_at" timestamp DEFAULT now(),
    CONSTRAINT "PK_6ad7b08e6491a02ebc9ed82019d" PRIMARY KEY ("id"),
    CONSTRAINT "UQ_2b3bfea1c7797e9d067dfc3c7a0" UNIQUE ("name")
) WITH (oids = false);

INSERT INTO "product_types" ("id", "name", "created_at", "updated_at")
VALUES (1, 'Cuenta de ahorros', '2021-06-20 23:54:38.908967', '2021-06-20 23:54:38.908967'),
       (2, 'Cuenta corriente', '2021-06-20 23:55:08.839727', '2021-06-20 23:55:08.839727'),
       (3, 'Leasing de vivienda', '2021-06-20 23:55:08.839727', '2021-06-20 23:55:08.839727'),
       (4, 'Tarjeta de crédito', '2021-06-20 23:55:08.839727', '2021-06-20 23:55:08.839727'),
       (5, 'Crédito ágil', '2021-06-20 23:55:08.839727', '2021-06-20 23:55:08.839727');

DROP TABLE IF EXISTS "transaction_details";
DROP SEQUENCE IF EXISTS transaction_details_id_seq;
CREATE SEQUENCE transaction_details_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."transaction_details"
(
    "id"             integer   DEFAULT nextval('transaction_details_id_seq') NOT NULL,
    "tax"            numeric                                                 NOT NULL,
    "created_at"     timestamp DEFAULT now()                                 NOT NULL,
    "updated_at"     timestamp DEFAULT now(),
    "transaction_id" integer,
    "description"    text,
    CONSTRAINT "PK_b9397af1203ca3a78ca6631e4b7" PRIMARY KEY ("id"),
    CONSTRAINT "REL_6334b14562af3c8a2fb29dab8a" UNIQUE ("transaction_id")
) WITH (oids = false);

INSERT INTO "transaction_details" ("id", "tax", "created_at", "updated_at", "transaction_id", "description")
VALUES (2, 4000, '2021-06-21 00:33:56.024867', '2021-06-21 00:33:56.024867', 1, 'Some description'),
       (3, 18000, '2021-06-21 01:01:02.135594', '2021-06-21 01:01:02.135594', 2, 'Some description');

DROP TABLE IF EXISTS "transaction_status";
DROP SEQUENCE IF EXISTS transaction_status_id_seq;
CREATE SEQUENCE transaction_status_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."transaction_status"
(
    "id"         integer   DEFAULT nextval('transaction_status_id_seq') NOT NULL,
    "name"       character varying                                      NOT NULL,
    "created_at" timestamp DEFAULT now()                                NOT NULL,
    "updated_at" timestamp DEFAULT now(),
    CONSTRAINT "PK_05fbbdf6bc1db819f47975c8c0b" PRIMARY KEY ("id"),
    CONSTRAINT "UQ_bdc1017b79532763afb7872a626" UNIQUE ("name")
) WITH (oids = false);

INSERT INTO "transaction_status" ("id", "name", "created_at", "updated_at")
VALUES (1, 'Pendiente', '2021-06-20 23:58:38.673077', '2021-06-20 23:58:38.673077'),
       (2, 'Aprobada', '2021-06-20 23:58:47.581385', '2021-06-20 23:58:47.581385'),
       (3, 'Rechazada', '2021-06-20 23:58:54.66755', '2021-06-20 23:58:54.66755');

DROP TABLE IF EXISTS "transactions";
DROP SEQUENCE IF EXISTS transactions_id_seq;
CREATE SEQUENCE transactions_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."transactions"
(
    "id"                    integer   DEFAULT nextval('transactions_id_seq') NOT NULL,
    "amount"                numeric                                          NOT NULL,
    "created_at"            timestamp DEFAULT now()                          NOT NULL,
    "updated_at"            timestamp DEFAULT now(),
    "account_id"            integer,
    "commerce_id"           integer,
    "transaction_status_id" integer,
    CONSTRAINT "PK_a219afd8dd77ed80f5a862f1db9" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "transactions" ("id", "amount", "created_at", "updated_at", "account_id", "commerce_id",
                            "transaction_status_id")
VALUES (1, 123000, '2021-06-21 00:00:34.907136', '2021-06-21 00:00:34.907136', 1, 1, 1),
       (2, 30000, '2021-06-21 00:03:05.734407', '2021-06-21 00:03:05.734407', 1, 1, 2),
       (3, 10000, '2021-06-21 00:22:40.63464', '2021-06-21 00:22:40.63464', 2, 1, 3),
       (4, 123000, '2021-06-21 03:15:02.309194', '2021-06-21 03:15:02.309194', 1, 1, 2),
       (5, 30000, '2021-06-21 14:13:32.393922', '2021-06-21 14:13:32.393922', 1, 1, 1);

ALTER TABLE ONLY "public"."accounts" ADD CONSTRAINT "FK_4562105493711e7489dda49fa1c" FOREIGN KEY (product_type_id) REFERENCES product_types(id) NOT DEFERRABLE;

ALTER TABLE ONLY "public"."product_requests" ADD CONSTRAINT "FK_c4cc82249a9469af28fab709c79" FOREIGN KEY (product_type_id) REFERENCES product_types(id) NOT DEFERRABLE;
ALTER TABLE ONLY "public"."product_requests" ADD CONSTRAINT "FK_f87a311365cb49b0b6110c8b585" FOREIGN KEY (product_request_status_id) REFERENCES product_request_status(id) NOT DEFERRABLE;

ALTER TABLE ONLY "public"."transaction_details" ADD CONSTRAINT "FK_6334b14562af3c8a2fb29dab8ac" FOREIGN KEY (transaction_id) REFERENCES transactions(id) NOT DEFERRABLE;

ALTER TABLE ONLY "public"."transactions" ADD CONSTRAINT "FK_49c0d6e8ba4bfb5582000d851f0" FOREIGN KEY (account_id) REFERENCES accounts(id) NOT DEFERRABLE;
ALTER TABLE ONLY "public"."transactions" ADD CONSTRAINT "FK_b5e07d789946c4e3ee03bb035a4" FOREIGN KEY (transaction_status_id) REFERENCES transaction_status(id) NOT DEFERRABLE;
ALTER TABLE ONLY "public"."transactions" ADD CONSTRAINT "FK_d700e5f9334ef75f061aacb3014" FOREIGN KEY (commerce_id) REFERENCES commerces(id) NOT DEFERRABLE;
