import express = require("express");
import { createContainer, asClass } from "awilix";
import { scopePerRequest } from "awilix-express";
import {AccountRepositoryImpl} from "../infrastructure/driven-adapters/typeorm/impl/account.repository.impl";
import {AccountUseCase} from "../domain/use-cases/account/account.usecase";
import {TransactionRepositoryImpl} from "../infrastructure/driven-adapters/typeorm/impl/transaction.repository.impl";
import {TransactionUseCase} from "../domain/use-cases/transaction/transaction.usecase";
import {ProductRepositoryImpl} from "../infrastructure/driven-adapters/typeorm/impl/product.repository.impl";
import {ProductUseCase} from "../domain/use-cases/product/product.usecase";
import {ProductRequestRepositoryImpl} from "../infrastructure/driven-adapters/typeorm/impl/productRequest.repository.impl";
import {ProductRequestUseCase} from "../domain/use-cases/product-request/productRequest.usecase";


export default (app: express.Application) => {
  const container = createContainer({
    injectionMode: "CLASSIC",
  });

  container.register({
    //driven adapters
    accountRepository: asClass(AccountRepositoryImpl).scoped(),
    transactionRepository: asClass(TransactionRepositoryImpl).scoped(),
    productRepository: asClass(ProductRepositoryImpl).scoped(),
    productRequestRepository: asClass(ProductRequestRepositoryImpl).scoped(),

    //use cases
    accountUseCase: asClass(AccountUseCase).scoped(),
    transactionUseCase: asClass(TransactionUseCase).scoped(),
    productUseCase: asClass(ProductUseCase).scoped(),
    productRequestUseCase: asClass(ProductRequestUseCase).scoped(),

  });

  app.use(scopePerRequest(container));
};
