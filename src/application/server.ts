import {app} from "./app";

const configDB = require("../infrastructure/driven-adapters/typeorm/config/db.config");

app.listen(5001, async () => {
    await configDB.startPostgres();
    console.log("server on port 5001");
});
