process.env.NODE_ENV = process.env.NODE_ENV || "development";

import path from "path";
import dotenv = require("dotenv");

dotenv.config({
  path: path.resolve(`${process.env.NODE_ENV}.env`),
});
console.log(process.env.NODE_ENV);

import express = require("express");
import { loadControllers } from "awilix-express";
import loadContainer from "./container";
import jwt from 'express-jwt';

const app: express.Application = express();

app.use(express.json());

loadContainer(app);

if (process.env.JWT_SECRET_KEY) {
    app.use(jwt({
        secret: process.env.JWT_SECRET_KEY,
        algorithms: ['HS256']
    }));
}

app.use(
  loadControllers("../infrastructure/entry-points/api/controllers/*.js", {
    cwd: __dirname,
  })
);

export { app };
