import {ApplicationException} from "../../../../common/exceptions/application.exception";
import {ProductRequestDto} from "../../../../common/dtos/productRequest.dto";
import {ProductRepository} from "../../../models/product/gateways/product.repository";

export class ProductRequestValidations {
    static validateIfProductRequestDtoIsValid(productRequest: ProductRequestDto) {
        if (!productRequest.productType) {
            throw new ApplicationException("productType is required.", 400, 2001);
        }
        if (!productRequest.userId) {
            throw new ApplicationException("userId is required.", 400, 2001);
        }
    }

    public static async validateIfProductTypeExists(productId: string, productRepository: ProductRepository) {
        const product = await productRepository.getProductById(productId);
        if (!product) throw new ApplicationException("Product not found.", 404, 1009);
    }
}
