import {ProductRepository} from "../../models/product/gateways/product.repository";
import {ProductRequest} from "../../models/product-request/productRequest";
import {ProductRequestRepository} from "../../models/product-request/gateways/productRequest.repository";
import {ProductRequestDto} from "../../../common/dtos/productRequest.dto";
import {AccountRepository} from "../../models/account/gateways/account.repository";
import {Transaction} from "../../models/transaction/transaction";
import {TransactionRepository} from "../../models/transaction/gateways/transaction.repository";
import {ProductRequestValidations} from "./utils/productRequest.validations";

export class ProductRequestUseCase {
    constructor(
        private readonly productRequestRepository: ProductRequestRepository,
        private readonly productRepository: ProductRepository,
        private readonly accountRepository: AccountRepository,
        private readonly transactionRepository: TransactionRepository,
    ) {
    }

    public async createProductRequest(productRequest: ProductRequestDto): Promise<ProductRequest> {
        ProductRequestValidations.validateIfProductRequestDtoIsValid(productRequest);
        await ProductRequestValidations.validateIfProductTypeExists(productRequest.productType, this.productRepository);
        const productRequestCreated = await this.productRequestRepository.createProductRequest(productRequest);
        const {userId} = productRequestCreated;
        productRequestCreated.products = await this.productRepository.getProductsApprovedByUserId(userId);
        productRequestCreated.transactions = await this.getTransactions(userId);
        return productRequestCreated;
    }

    private async getTransactions(userId: string) {
        const accounts = await this.accountRepository.getAccountsByUserId(userId);
        let transactions: Transaction[] = [];
        if (accounts) {
            for (const account of accounts) {
                const accountTransactions = await this.transactionRepository.getTransactionsByAccountId(String(account.id))
                if (accountTransactions) {
                    transactions = transactions.concat(accountTransactions);
                }
            }
        }
        return transactions;
    }
}
