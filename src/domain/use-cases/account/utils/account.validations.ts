import {ApplicationException} from "../../../../common/exceptions/application.exception";
import {Account} from "../../../models/account/account";

export class AccountValidations {
    static validateIfUserIdIsValid(userId: string) {
        if (!userId) {
            throw new ApplicationException("User identification is required.", 400, 2001);
        }
    }

    static validateIfAccountExist(accounts: Account[] | null) {
        if (accounts === null) {
            throw new ApplicationException("User identification doesn't have any account.", 404, 1002);
        }
    }
}
