import {AccountRepository} from "../../models/account/gateways/account.repository";
import {Account} from "../../models/account/account";
import {AccountValidations} from "./utils/account.validations";

export class AccountUseCase {
    constructor(private readonly accountRepository: AccountRepository) {
    }

    public async getAccountsByUserId(userId: string): Promise<Account[] | null> {
        AccountValidations.validateIfUserIdIsValid(userId);
        const accounts = await this.accountRepository.getAccountsByUserId(userId);
        AccountValidations.validateIfAccountExist(accounts);
        return accounts;
    }
}
