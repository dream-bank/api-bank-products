import {ApplicationException} from "../../../../common/exceptions/application.exception";
import {Transaction, TransactionDetail} from "../../../models/transaction/transaction";
import {AccountRepository} from "../../../models/account/gateways/account.repository";
import {TransactionRepository} from "../../../models/transaction/gateways/transaction.repository";

export class TransactionValidations {

    static validateIfRangeDateIsValid(startDate: string | undefined, endDate: string | undefined) {
        const regex = /^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/;
        if (startDate && !regex.test(startDate)) {
            throw new ApplicationException("from is incorrectly formatted.", 400, 2012);
        }
        if (endDate && !regex.test(endDate)) {
            throw new ApplicationException("to is incorrectly formatted.", 400, 2012);
        }
    }

    static validateIfAccountIsValid(accountId: string) {
        if (!accountId) {
            throw new ApplicationException("accountId is required.", 400, 2001);
        }
    }

    static validateIfTransactionIsValid(transactionId: string) {
        if (!transactionId) {
            throw new ApplicationException("transactionId is required.", 400, 2002);
        }
    }

    public static validateIfAccountHaveTransactions(transactions: Transaction[] | null) {
        if (transactions === null) {
            throw new ApplicationException("Account doesn't have any transaction.", 404, 1003);
        }
    }

    public static validateIfTransactionHaveDetails(details: TransactionDetail | null) {
        if (details === null) {
            throw new ApplicationException("Transaction doesn't have any detail.", 404, 1004);
        }
    }

    public static async validateIfAccountExists(accountId: string, accountRepository: AccountRepository) {
        const account = await accountRepository.getAccountById(accountId);
        if (!account) throw new ApplicationException("Account not found.", 404, 1005);
    }

    public static async validateIfTransactionExists(transactionId: string, transactionRepository: TransactionRepository) {
        const transaction = await transactionRepository.getTransactionsById(transactionId);
        if (!transaction) throw new ApplicationException("Transaction not found.", 404, 1006);
    }
}
