import {TransactionRepository} from "../../models/transaction/gateways/transaction.repository";
import {
    Transaction,
    TransactionDetail
} from "../../models/transaction/transaction";
import {AccountRepository} from "../../models/account/gateways/account.repository";
import {TransactionValidations} from "./utils/transaction.validations";

export class TransactionUseCase {
    constructor(
        private readonly transactionRepository: TransactionRepository,
        private readonly accountRepository: AccountRepository
    ) {
    }

    public async getTransactionsByAccountId(accountId: string): Promise<Transaction[] | null> {
        TransactionValidations.validateIfAccountIsValid(accountId);
        await TransactionValidations.validateIfAccountExists(accountId, this.accountRepository);
        const transactions = await this.transactionRepository.getTransactionsByAccountId(accountId);
        TransactionValidations.validateIfAccountHaveTransactions(transactions);
        return transactions
    }

    public async getTransactionsDetail(transactionId: string): Promise<TransactionDetail | null> {
        TransactionValidations.validateIfTransactionIsValid(transactionId);
        await TransactionValidations.validateIfTransactionExists(transactionId, this.transactionRepository);
        const details = await this.transactionRepository.getTransactionsDetail(transactionId);
        TransactionValidations.validateIfTransactionHaveDetails(details);
        return details;
    }

    public async getAverageTransactionsAmountByAccountId(
        accountId: string, startDate: string | undefined, endDate: string | undefined
    ): Promise<number | null> {
        TransactionValidations.validateIfRangeDateIsValid(startDate, endDate);
        await TransactionValidations.validateIfAccountExists(accountId, this.accountRepository);
        const transactionAmounts = await this.transactionRepository
            .getValidTransactionsAmountsByAccountIdAndDate(accountId, startDate, endDate);
        let averageTransactionAmount: number = 0;
        if (transactionAmounts?.length) {
            for (const transactionAmount of transactionAmounts) {
                averageTransactionAmount += Number(transactionAmount.amount);
            }
            return averageTransactionAmount / transactionAmounts.length;
        }
        return 0;
    }
}

