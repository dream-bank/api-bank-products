import {Product} from "../../models/product/product";
import {ProductRepository} from "../../models/product/gateways/product.repository";

export class ProductUseCase {
    constructor(private readonly productRepository: ProductRepository) {
    }
    public async getProducts(): Promise<Product[]> {
        return await this.productRepository.getProducts();
    }
}
