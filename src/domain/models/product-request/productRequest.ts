import {Transaction} from "../transaction/transaction";
import {Product} from "../product/product";

export interface ProductRequest {
    id: number;
    status: string;
    userId: string;
    productType: string;
    transactions: Transaction[];
    products: Product[];
    createdAt: Date;
    updatedAt: Date | null;
}
