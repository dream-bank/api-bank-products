import {ProductRequest} from "../productRequest";
import {ProductRequestDto} from "../../../../common/dtos/productRequest.dto";

export interface ProductRequestRepository {
    createProductRequest(productRequest: ProductRequestDto): Promise<ProductRequest>;
}
