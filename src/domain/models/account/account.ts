export interface Account {
  id: number;
  balance: number;
  userId: string;
  productType: string;
  createdAt: Date;
  updatedAt: Date | null;
}
