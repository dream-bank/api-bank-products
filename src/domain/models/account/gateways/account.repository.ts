import {Account} from "../account";

export interface AccountRepository {
    getAccountsByUserId(userId: string): Promise<Account[] | null>;
    getAccountById(accountId: string): Promise<Account | null>;
}
