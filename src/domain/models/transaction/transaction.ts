export interface Transaction {
    id: number;
    amount: number;
    status: string;
    commerce: string;
    account: number;
    createdAt?: Date;
    updatedAt?: Date | null;
}

export interface TransactionDetail {
    id: number;
    tax: number;
    description: string;
    amount: number;
    status: string;
    commerce: string;
    createdAt: Date;
    updatedAt: Date | null;
}

export interface TransactionAverageAmount {
    average: number;
}



