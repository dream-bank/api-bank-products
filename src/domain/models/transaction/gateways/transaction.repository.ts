import {Transaction, TransactionDetail} from "../transaction";
import {TransactionAmountsDto} from "../../../../common/dtos/transaction.dto";

export interface TransactionRepository {
    getTransactionsByAccountId(accountId: string): Promise<Transaction[] | null>;

    getTransactionsById(transactionId: string): Promise<Transaction | null>;

    getTransactionsDetail(transactionId: string): Promise<TransactionDetail | null>;

    getValidTransactionsAmountsByAccountIdAndDate(
        accountId: string, startDate: string | undefined, endDate: string | undefined)
        : Promise<TransactionAmountsDto[] | null>;
}
