import {Product} from "../product";

export interface ProductRepository {
    getProducts(): Promise<Product[]>;

    getProductById(productId: string): Promise<Product | null>;

    getProductsApprovedByUserId(userId: string): Promise<Product[]>;
}
