export interface Product {
    id: number;
    name: string;
    createdAt: Date;
    updatedAt: Date | null;
}

