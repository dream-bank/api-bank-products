import {createConnection} from "typeorm";

const startPostgres = async () => {
    try {
        await createConnection({
            type: 'postgres',
            host: process.env.DB_POSTGRES_HOST,
            port: Number(process.env.DB_POSTGRES_PORT),
            username: process.env.DB_POSTGRES_USER,
            password: process.env.DB_POSTGRES_PASSWORD,
            database: process.env.DB_POSTGRES_DATABASE,
            logging: ["error"],
            synchronize: true,
            entities: [__dirname + '/../**/*.entity{.ts,.js}'],
            migrations: [__dirname + "/../**/migration/**"],
        });
        console.log('Database postgres connected')
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    startPostgres,
};
