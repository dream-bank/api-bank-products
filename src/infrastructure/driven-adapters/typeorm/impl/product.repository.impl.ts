import {getConnection} from "typeorm";
import {ProductRepository} from "../../../../domain/models/product/gateways/product.repository";
import {Product} from "../../../../domain/models/product/product";
import {ProductTypeEntity} from "../entity/productType.entity";
import {ProductTypeMapToDomain} from "./common/utils/productTypeMapToDomain";
import {ProductRequestEntity} from "../entity/productRequest.entity";

export class ProductRepositoryImpl implements ProductRepository {
    async getProductById(productId: string): Promise<Product | null> {
        const productTypes = await getConnection()
            .getRepository(ProductTypeEntity)
            .createQueryBuilder("productType")
            .where({id: productId})
            .getOne();
        if (productTypes) {
            return ProductTypeMapToDomain.productTypeMapToDomain(productTypes);
        }
        return null;
    }

    async getProducts(): Promise<Product[]> {
        const productTypes = await getConnection()
            .getRepository(ProductTypeEntity)
            .createQueryBuilder("productType")
            .getMany();
        return ProductTypeMapToDomain.productTypesMapToDomain(productTypes);
    }

    async getProductsApprovedByUserId(userId: string): Promise<Product[]> {
        const productTypes = await getConnection()
            .getRepository(ProductRequestEntity)
            .createQueryBuilder("productRequest")
            .innerJoinAndSelect("productRequest.productType", "productType")
            .innerJoinAndSelect("productRequest.status", "status")
            .where({userId})
            .andWhere("status.id = :id", {id: 2})
            .getMany();
        return ProductTypeMapToDomain.productRequestEntityMapToDomain(productTypes);
    }
}
