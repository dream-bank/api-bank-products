import {AccountEntity} from "../../../entity/account.entity";
import {Account} from "../../../../../../domain/models/account/account";

export class AccountMapToDomain {
    public static accountsMapToDomain(accountEntities: AccountEntity[]) {
        const accounts: Account[] = []
        for (const account of accountEntities) {
            accounts.push(this.accountMapToDomain(account));
        }
        return accounts;
    }

    public static accountMapToDomain(accountEntity: AccountEntity) {
        const {id, balance, userId, productType, createdAt, updatedAt} = accountEntity;
        const account: Account = {
            id,
            balance,
            userId,
            productType: productType.name,
            createdAt,
            updatedAt
        };
        return account;
    }
}
