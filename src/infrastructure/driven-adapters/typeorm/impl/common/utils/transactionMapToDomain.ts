import {
    Transaction,
    TransactionDetail
} from "../../../../../../domain/models/transaction/transaction";
import {TransactionEntity} from "../../../entity/transaction.entity";
import {TransactionDetailEntity} from "../../../entity/transactionDetail.entity";
import {TransactionAmountsDto} from "../../../../../../common/dtos/transaction.dto";

export class TransactionMapToDomain {
    public static transactionsMapToDomain(transactionEntities: TransactionEntity[]) {
        const transactions: Transaction[] = []
        for (const transaction of transactionEntities) {
            transactions.push(this.transactionMapToDomain(transaction));
        }
        return transactions;
    }

    public static transactionMapToDomain(transactionEntities: TransactionEntity) {
        const {id, amount, commerce, status, account, createdAt, updatedAt} = transactionEntities;
        const transaction: Transaction = {
            id,
            amount,
            commerce: commerce.name,
            status: status.name,
            account: account.id,
            createdAt,
            updatedAt
        };
        return transaction;
    }

    public static transactionDetailMapToDomain(transactionDetailEntity: TransactionDetailEntity) {
        const {id, tax, description, transaction, createdAt, updatedAt} = transactionDetailEntity;
        return {
            id,
            tax,
            description,
            amount: transaction.amount,
            commerce: transaction.commerce.name,
            status: transaction.status.name,
            createdAt,
            updatedAt
        } as TransactionDetail;
    }

    public static averageTransactionsAmountMapToDomain(transactionEntities: TransactionEntity[]) {
        const averageTransactions: TransactionAmountsDto[] = []
        for (const transaction of transactionEntities) {
            const {amount} = transaction;
            averageTransactions.push({amount});
        }
        return averageTransactions;
    }
}
