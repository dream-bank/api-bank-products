import {ProductTypeEntity} from "../../../entity/productType.entity";
import {Product} from "../../../../../../domain/models/product/product";
import {ProductRequestEntity} from "../../../entity/productRequest.entity";

export class ProductTypeMapToDomain {
    public static productTypesMapToDomain(productTypeEntities: ProductTypeEntity[]) {
        const products: Product[] = []
        for (const productTypeEntity of productTypeEntities) {
            products.push(this.productTypeMapToDomain(productTypeEntity));
        }
        return products;
    }

    public static productTypeMapToDomain(productTypeEntity: ProductTypeEntity) {
        const {id, name, createdAt, updatedAt} = productTypeEntity;
        const product: Product = {
            id,
            name,
            createdAt,
            updatedAt
        };
        return product;
    }

    public static productRequestEntityMapToDomain(productRequestEntities: ProductRequestEntity[]) {
        const products: Product[] = []
        for (const productRequestEntity of productRequestEntities) {
            const {productType} = productRequestEntity;
            const {id, name, createdAt, updatedAt } = productType;
            const product: Product = {
                id,
                name,
                createdAt,
                updatedAt
            };
            products.push(product);
        }
        return products;
    }
}
