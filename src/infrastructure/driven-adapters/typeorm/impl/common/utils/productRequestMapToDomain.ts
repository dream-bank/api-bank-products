import {ProductRequest} from "../../../../../../domain/models/product-request/productRequest";
import {ProductRequestEntity} from "../../../entity/productRequest.entity";

export class ProductRequestMapToDomain {
    public static productRequestMapToDomain(productRequestEntity: ProductRequestEntity) {
        const {id, status, productType, userId, createdAt, updatedAt} = productRequestEntity;
        const productRequest: ProductRequest = {
            id,
            status: status.name,
            productType: productType.name,
            userId,
            products: [],
            transactions: [],
            createdAt,
            updatedAt
        }
        return productRequest;
    }
}
