import {ProductTypeEntity} from "../entity/productType.entity";
import {ProductRequestRepository} from "../../../../domain/models/product-request/gateways/productRequest.repository";
import {ProductRequestDto} from "../../../../common/dtos/productRequest.dto";
import {ProductRequest} from "../../../../domain/models/product-request/productRequest";
import {ProductRequestEntity} from "../entity/productRequest.entity";
import {ProductRequestMapToDomain} from "./common/utils/productRequestMapToDomain";
import {ProductRequestStatusEntity} from "../entity/productRequestStatus.entity";

export class ProductRequestRepositoryImpl implements ProductRequestRepository {
    async createProductRequest(productRequestDto: ProductRequestDto): Promise<ProductRequest> {
        const {productType, userId} = productRequestDto;
        const productTypeEntity = await ProductTypeEntity.findOne(productType);
        const status = await ProductRequestStatusEntity.findOne(1);
        const newProductRequestEntity = ProductRequestEntity.create({
            userId,
            productType: productTypeEntity,
            status,
        });
        const productRequestCreated = await newProductRequestEntity.save();
        return ProductRequestMapToDomain.productRequestMapToDomain(productRequestCreated)
    }
}
