import {getConnection} from "typeorm";
import {TransactionRepository} from "../../../../domain/models/transaction/gateways/transaction.repository";
import {Transaction, TransactionDetail} from "../../../../domain/models/transaction/transaction";
import {TransactionEntity} from "../entity/transaction.entity";
import {TransactionMapToDomain} from "./common/utils/transactionMapToDomain";
import {TransactionDetailEntity} from "../entity/transactionDetail.entity";
import {TransactionAmountsDto} from "../../../../common/dtos/transaction.dto";

export class TransactionRepositoryImpl implements TransactionRepository {
    async getTransactionsByAccountId(accountId: string): Promise<Transaction[] | null> {
        const accountTransactions = await getConnection()
            .getRepository(TransactionEntity)
            .createQueryBuilder("transaction")
            .innerJoinAndSelect("transaction.commerce", "commerce")
            .innerJoinAndSelect("transaction.account", "account")
            .innerJoinAndSelect("transaction.status", "status")
            .where("account.id = :id", {id: accountId})
            .getMany();
        if (accountTransactions.length) {
            return TransactionMapToDomain.transactionsMapToDomain(accountTransactions);
        }
        return null;
    }

    async getTransactionsById(TransactionId: string): Promise<Transaction | null> {
        const accountTransactions = await getConnection()
            .getRepository(TransactionEntity)
            .createQueryBuilder("transaction")
            .innerJoinAndSelect("transaction.commerce", "commerce")
            .innerJoinAndSelect("transaction.account", "account")
            .innerJoinAndSelect("transaction.status", "status")
            .where("transaction.id = :id", {id: TransactionId})
            .getOne();
        if (accountTransactions) {
            return TransactionMapToDomain.transactionMapToDomain(accountTransactions);
        }
        return null;
    }

    async getTransactionsDetail(transactionId: string): Promise<TransactionDetail | null> {
        const transactionDetail = await getConnection()
            .getRepository(TransactionDetailEntity)
            .createQueryBuilder("transactionDetails")
            .innerJoinAndSelect("transactionDetails.transaction", "transaction")
            .innerJoinAndSelect("transaction.commerce", "commerce")
            .innerJoinAndSelect("transaction.status", "status")
            .where("transaction.id = :id", {id: transactionId})
            .getOne();
        if (transactionDetail) {
            return TransactionMapToDomain.transactionDetailMapToDomain(transactionDetail);
        }
        return null;
    }

    async getValidTransactionsAmountsByAccountIdAndDate(accountId: string, startDate: string, endDate: string)
        : Promise<TransactionAmountsDto[] | null> {
        const query = getConnection()
            .getRepository(TransactionEntity)
            .createQueryBuilder("transaction")
            .innerJoin("transaction.account", "account")
            .select(["transaction.amount"])
            .where("account.id = :id", {id: accountId})
            .andWhere("transaction.status = :status", {status: 2});
        if (startDate) {
            query.andWhere("transaction.createdAt >= :startDate", {startDate: `${startDate} 00:00`})
        }
        if (endDate) {
            query.andWhere("transaction.updatedAt <= :endDate", {endDate: `${endDate} 23:59`})
        }
        const accountTransactions = await query.getMany();
        if (accountTransactions.length) {
            return TransactionMapToDomain.averageTransactionsAmountMapToDomain(accountTransactions);
        }
        return null;
    }
}
