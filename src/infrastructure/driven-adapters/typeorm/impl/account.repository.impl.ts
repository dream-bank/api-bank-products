import {AccountRepository} from "../../../../domain/models/account/gateways/account.repository";
import {Account} from "../../../../domain/models/account/account";
import {getConnection} from "typeorm";
import {AccountEntity} from "../entity/account.entity";
import {AccountMapToDomain} from "./common/utils/accountMapToDomain";

export class AccountRepositoryImpl implements AccountRepository {
    async getAccountsByUserId(userId: string): Promise<Account[] | null> {
        const userAccounts = await getConnection()
            .getRepository(AccountEntity)
            .createQueryBuilder("account")
            .innerJoinAndSelect("account.productType", "productType")
            .where({userId})
            .getMany();

        if (userAccounts.length) {
            return AccountMapToDomain.accountsMapToDomain(userAccounts);
        }
        return null;
    }

    async getAccountById(accountId: string): Promise<Account | null> {
        const userAccount = await getConnection()
            .getRepository(AccountEntity)
            .createQueryBuilder("account")
            .innerJoinAndSelect("account.productType", "productType")
            .where({id: accountId})
            .getOne();
        if (userAccount) {
            return AccountMapToDomain.accountMapToDomain(userAccount);
        }
        return null;
    }
}
