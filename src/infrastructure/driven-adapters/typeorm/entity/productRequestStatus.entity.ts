import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn, Unique,
    UpdateDateColumn
} from "typeorm";
import {IsNotEmpty} from "class-validator";
import {ProductRequestEntity} from "./productRequest.entity";

@Entity("product_request_status")
@Unique(["name"])
export class ProductRequestStatusEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @Column({
        type: "varchar",
        nullable: false
    })
    name: string;

    @CreateDateColumn({name: "created_at"})
    createdAt: Date;

    @UpdateDateColumn({name: "updated_at", nullable: true})
    updatedAt: Date;

    @OneToMany(() => ProductRequestEntity, productRequests => productRequests.status)
    productRequests: ProductRequestEntity[];
}
