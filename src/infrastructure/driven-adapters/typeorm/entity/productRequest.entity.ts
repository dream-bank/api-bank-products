import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {IsNotEmpty} from "class-validator";
import {ProductTypeEntity} from "./productType.entity";
import {ProductRequestStatusEntity} from "./productRequestStatus.entity";

@Entity("product_requests")
export class ProductRequestEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @Column({
        type: "varchar",
        length: 20,
        nullable: false,
        name: "user_id"
    })
    userId: string;

    @CreateDateColumn({name: "created_at"})
    createdAt: Date;

    @UpdateDateColumn({name: "updated_at", nullable: true})
    updatedAt: Date;

    @ManyToOne(() => ProductRequestStatusEntity, productRequestStatus => productRequestStatus.productRequests)
    @JoinColumn({name: "product_request_status_id"})
    status: ProductRequestStatusEntity;

    @ManyToOne(() => ProductTypeEntity, productType => productType.productRequests)
    @JoinColumn({name: "product_type_id"})
    productType: ProductTypeEntity;
}
