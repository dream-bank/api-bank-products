import {
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne, OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {IsNotEmpty} from "class-validator";
import {ProductTypeEntity} from "./productType.entity";
import {TransactionEntity} from "./transaction.entity";

@Entity( "accounts")
export class AccountEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @Column({
        type: "decimal",
        nullable: false
    })
    balance: number;

    @IsNotEmpty()
    @Column({
        type: "varchar",
        length: 20,
        nullable: false,
        name: "user_id"
    })
    userId: string;

    @CreateDateColumn({name: "created_at"})
    createdAt: Date;

    @UpdateDateColumn({name: "updated_at", nullable: true})
    updatedAt: Date;

    @ManyToOne(() => ProductTypeEntity, productType => productType.accounts)
    @JoinColumn({name: "product_type_id"})
    productType: ProductTypeEntity;

    @OneToMany(() => TransactionEntity, transaction => transaction.account)
    transactions: TransactionEntity[];
}
