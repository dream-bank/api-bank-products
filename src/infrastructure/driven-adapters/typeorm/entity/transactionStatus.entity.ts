import {
    Column,
    CreateDateColumn,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn, Unique,
    UpdateDateColumn
} from "typeorm";
import {IsNotEmpty} from "class-validator";
import {TransactionEntity} from "./transaction.entity";

@Entity("transaction_status")
@Unique(["name"])
export class TransactionStatusEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @Column({
        type: "varchar",
        nullable: false
    })
    name: string;

    @CreateDateColumn({name: "created_at"})
    createdAt: Date;

    @UpdateDateColumn({name: "updated_at", nullable: true})
    updatedAt: Date;

    @OneToMany(() => TransactionEntity, transactions => transactions.status)
    transactions: TransactionEntity[];
}
