import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn, Unique,
    UpdateDateColumn
} from "typeorm";
import {IsNotEmpty} from "class-validator";
import {AccountEntity} from "./account.entity";
import {ProductRequestEntity} from "./productRequest.entity";

@Entity("product_types")
@Unique(["name"])
export class ProductTypeEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @Column({
        type: "varchar",
        nullable: false
    })
    name: string;

    @CreateDateColumn({name: "created_at"})
    createdAt: Date;

    @UpdateDateColumn({name: "updated_at", nullable: true})
    updatedAt: Date;

    @OneToMany(() => AccountEntity, account => account.productType)
    accounts: AccountEntity[];

    @OneToMany(() => ProductRequestEntity, productRequest => productRequest.productType)
    productRequests: ProductRequestEntity[];
}
