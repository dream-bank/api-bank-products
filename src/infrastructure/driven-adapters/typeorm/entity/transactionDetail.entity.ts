import {
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {IsNotEmpty} from "class-validator";
import {TransactionEntity} from "./transaction.entity";

@Entity("transaction_details")
export class TransactionDetailEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @Column({
        type: "decimal",
        nullable: false
    })
    tax: number;

    @Column({
        type: "text",
        nullable: true
    })
    description: string;

    @CreateDateColumn({name: "created_at"})
    createdAt: Date;

    @UpdateDateColumn({name: "updated_at", nullable: true})
    updatedAt: Date;

    @OneToOne(() => TransactionEntity, transaction => transaction.transactionDetail)
    @JoinColumn({name: "transaction_id"})
    transaction: TransactionEntity;
}
