import {
    Column,
    CreateDateColumn,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn, Unique,
    UpdateDateColumn
} from "typeorm";
import {IsNotEmpty} from "class-validator";
import {TransactionEntity} from "./transaction.entity";

@Entity("commerces")
@Unique(["name"])
export class CommerceEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @Column({
        type: "varchar",
        nullable: false
    })
    name: string;

    @CreateDateColumn({name: "created_at"})
    createdAt: Date;

    @UpdateDateColumn({name: "updated_at", nullable: true})
    updatedAt: Date;

    @OneToMany(() => TransactionEntity, transactions => transactions.commerce)
    transactions: TransactionEntity[];
}
