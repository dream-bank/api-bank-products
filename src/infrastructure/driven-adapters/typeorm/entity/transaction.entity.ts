import {
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne, OneToOne, PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {IsNotEmpty} from "class-validator";
import {AccountEntity} from "./account.entity";
import {CommerceEntity} from "./commerce.entity";
import {TransactionDetailEntity} from "./transactionDetail.entity";
import {TransactionStatusEntity} from "./transactionStatus.entity";

@Entity("transactions")
export class TransactionEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @Column({
        type: "decimal",
        nullable: false
    })
    amount: number;

    @CreateDateColumn({name: "created_at"})
    createdAt: Date;

    @UpdateDateColumn({name: "updated_at", nullable: true})
    updatedAt: Date;

    @ManyToOne(() => AccountEntity, account => account.transactions)
    @JoinColumn({name: "account_id"})
    account: AccountEntity;

    @ManyToOne(() => CommerceEntity, commerce => commerce.transactions)
    @JoinColumn({name: "commerce_id"})
    commerce: CommerceEntity;

    @ManyToOne(() => TransactionStatusEntity, transactionStatus => transactionStatus.transactions)
    @JoinColumn({name: "transaction_status_id"})
    status: TransactionStatusEntity;

    @OneToOne(() => TransactionDetailEntity)
    transactionDetail: TransactionDetailEntity;
}
