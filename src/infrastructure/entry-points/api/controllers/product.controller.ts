import {Request, Response} from "express";
import {BaseController} from "./common/base.controller";
import {GET, route} from "awilix-express";
import {ProductUseCase} from "../../../../domain/use-cases/product/product.usecase";

@route("/api/v1/products")
export class ProductController extends BaseController {
    constructor(private readonly productUseCase: ProductUseCase) {
        super();
    }

    @GET()
    public async getProducts(req: Request, res: Response) {
        try {
            const products = await this.productUseCase.getProducts();
            return res.json({products});
        } catch (error) {
            this.handleException(error, res);
        }
    }
}
