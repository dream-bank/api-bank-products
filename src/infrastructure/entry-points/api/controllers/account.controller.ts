import {Request, Response} from "express";
import {BaseController} from "./common/base.controller";
import {AccountUseCase} from "../../../../domain/use-cases/account/account.usecase";
import {GET, route} from "awilix-express";

@route("/api/v1/accounts")
export class AccountController extends BaseController {
    constructor(private readonly accountUseCase: AccountUseCase) {
        super();
    }

    @GET()
    @route("/users/:id")
    public async getAccountsByUserId(req: Request, res: Response) {
        try {
            const {id} = req.params;
            const accounts = await this.accountUseCase.getAccountsByUserId(id);
            return res.json({accounts});
        } catch (error) {
            this.handleException(error, res);
        }
    }
}
