import {Request, Response} from "express";
import {BaseController} from "./common/base.controller";
import {GET, route} from "awilix-express";
import {TransactionUseCase} from "../../../../domain/use-cases/transaction/transaction.usecase";

@route("/api/v1")
export class TransactionController extends BaseController {
    constructor(private readonly transactionUseCase: TransactionUseCase) {
        super();
    }

    @GET()
    @route("/accounts/:id/transactions")
    public async getTransactionsByAccountId(req: Request, res: Response) {
        try {
            const {id} = req.params;
            const transactions = await this.transactionUseCase.getTransactionsByAccountId(id);
            return res.json({transactions});
        } catch (error) {
            this.handleException(error, res);
        }
    }

    @GET()
    @route("/transactions/:id/details")
    public async getTransactionsDetail(req: Request, res: Response) {
        try {
            const {id} = req.params;
            const transactionsDetail = await this.transactionUseCase.getTransactionsDetail(id);
            if (transactionsDetail) {
                return res.json({transactionsDetail});
            }
            return res.status(404).json({errors: "Transaction doesn't have any detail."});
        } catch (error) {
            this.handleException(error, res);
        }
    }

    @GET()
    @route("/accounts/:id/transactions/averages")
    public async getAverageTransactionsAmountByAccountId(req: Request, res: Response) {
        try {
            const {id} = req.params;
            const startDate: any = req.query.from;
            const endDate: any = req.query.to;
            const averageTransactions = await this.transactionUseCase
                .getAverageTransactionsAmountByAccountId(id, startDate, endDate);
            return res.json({averageTransactions});
        } catch (error) {
            this.handleException(error, res);
        }
    }
}
