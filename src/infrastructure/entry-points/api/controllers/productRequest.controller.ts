import {Request, Response} from "express";
import {BaseController} from "./common/base.controller";
import {POST, route} from "awilix-express";
import {ProductRequestUseCase} from "../../../../domain/use-cases/product-request/productRequest.usecase";
import {ProductRequestDto} from "../../../../common/dtos/productRequest.dto";

@route("/api/v1/product-requests")
export class ProductController extends BaseController {
    constructor(private readonly productRequestUseCase: ProductRequestUseCase) {
        super();
    }

    @POST()
    public async createProductRequests(req: Request, res: Response) {
        try {
            const {userId, productType} = req.body;
            const productRequest = await this.productRequestUseCase.createProductRequest({
                userId,
                productType
            } as ProductRequestDto);
            res.status(201).json({productRequest});
        } catch (error) {
            this.handleException(error, res);
        }
    }
}
