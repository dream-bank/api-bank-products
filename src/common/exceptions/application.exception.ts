export class ApplicationException extends Error {
    httpCode: number;
    internalCode: number;
    constructor(message: string = "An unexpected error occurred.", code: number = 400, internalCode: number = 1001) {
        super(message);
        this.httpCode = code;
        this.internalCode = internalCode;
    }
}
