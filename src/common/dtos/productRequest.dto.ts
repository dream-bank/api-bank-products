export interface ProductRequestDto {
    userId: string;
    productType: string;
}
